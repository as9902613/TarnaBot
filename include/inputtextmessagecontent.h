#ifndef INPUTTEXTMESSAGECONTENT_H
#define INPUTTEXTMESSAGECONTENT_H

#include <QString>

#include "inputmessagecontent.h"

namespace Telegram
{
    class InputTextMessageContent : public InputMessageContent
    {
    public:
        InputTextMessageContent();
        InputTextMessageContent(QJsonObject obj);
        
        //Getters/Setters
        QString getMessageText() const;
        void setMessageText(const QString &value);
        
        QString getParseMode() const;
        void setParseMode(const QString &value);
        
        bool getDisableWebPagePreview() const;
        void setDisableWebPagePreview(bool value);
        
        //Flag getters
        bool hasMessageText() const;
        
        bool hasParseMode() const;
        
    private:
        QString messageText;
        QString parseMode;
        
        bool disableWebPagePreview;
        
        //Flags
        bool _hasMessageText;
        bool _hasParseMode;
    };
}
#endif // INPUTTEXTMESSAGECONTENT_H
